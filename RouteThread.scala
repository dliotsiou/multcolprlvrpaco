package projectMultColPRL

class RouteThread(var pheroMIndex: Int, var ants: List[Ant]) extends Thread {

  this.pheroMIndex = pheroMIndex //routeIndex from aco
  this.ants = ants

  var dimension: Int = VRP.aco.dimension
  var alpha: Double = VRP.aco.alpha
  var t0: Double = VRP.aco.t0
  val distMatrixArray = VRP.distMatrixArray
  var depot = VRP.depot

  var pheroMVector: java.util.Vector[Array[Array[WrapDouble]]] = null
  var hetaMatrix: List[List[Double]] = Nil

  def setParams(hetaMatrix: List[List[Double]], pheroMVector: java.util.Vector[Array[Array[WrapDouble]]]) = {
    this.pheroMVector = pheroMVector
    this.hetaMatrix = hetaMatrix
  }

  override def run() = { //TODO: iterations after the 1st: k.eligibleNodes and nodesExcDepots is empty

    var nextThread: RouteThread = null

    //    var eligibleNodes: List[Int] = Nil

    var routePheroMatrix = Array.ofDim[WrapDouble](dimension, dimension)

    try {
      routePheroMatrix = pheroMVector.get(pheroMIndex)
    } catch {
      case indexoutofbounds: ArrayIndexOutOfBoundsException =>

        for (i <- 0 to (dimension - 1)) {
          var phero1DArray = Array.ofDim[WrapDouble](dimension)
          for (j <- 0 to (dimension - 1)) {
            val phero = t0
            phero1DArray(j) = new WrapDouble(phero) //don't need to reverse
          }
          routePheroMatrix(i) = phero1DArray //don't need to reverse
        }

        pheroMVector.add(pheroMIndex, routePheroMatrix) //add routePheroMatrix to pheroMList, at index = routeIndex
    }

    ants.foreach { k =>

      System.out.println("This is ant " + k)

      k.synchronized {
        //stuff from Seq ant loop:

        System.out.println("")
        System.out.println("Ant: " + k)

        k.nextDestination = depot // node 1 is the depot, nodeNorm 0 is the depot
        k.setLocation(depot)


        //        while (k.visitedNodes.distinct.length < dimension & k.eligibleNodes != Nil) { //until one full solution is completed

        //capacity-related stuff:
        k.location = depot
        //          k.visitedNodes = k.location :: k.visitedNodes
        k.visitedNodesSet.add(k.location)
        k.visitedNodesVector.add(k.location)

        k.eligibleNodesSet.retain(n => !k.visitedNodesSet.contains(n)) //TODO: Probably redundant.
        k.eligibleNodesArray = k.eligibleNodesArray.filterNot(n => k.visitedNodesSet.contains(n))
        
        k.load = 0
        //          routeIndex = routeIndex + 1

        var routeNodes: List[Int] = Nil

        while (k.load <= k.capacity & k.visitedNodesSet.size < dimension & !k.eligibleNodesSet.isEmpty) { //i.e. for each route
          //System.err.println("k.visitedNodes.distinct.length: "+k.visitedNodes.distinct.length)
          k.nextDestination = VRP.aco.chooseNext(k, routePheroMatrix, hetaMatrix)
          k.load += VRP.nodeDemandArray(k.nextDestination) //No off-by-1.

          if (k.load <= k.capacity) { //if node doesn't overflow the limit, go there
            k.setLocation(k.nextDestination) //move to next customer - set location to nextDestination chosen in previous step
            //              k.visitedNodes = k.location :: k.visitedNodes
            k.visitedNodesSet.add(k.location)
            k.visitedNodesVector.add(k.location) //TODO: check added at the right place!

            routeNodes = k.location :: routeNodes
            k.eligibleNodesSet.remove(k.location) //NEW
            k.eligibleNodesArray = k.eligibleNodesArray.filterNot(n => n == k.location)
          }
        } //end of inner while (for each route)

        routeNodes = routeNodes.reverse

        // HERE: call 2-opt, if enabled:
        if (VRP.flag2opt == 1) {
          val oldRouteNodes = routeNodes
          routeNodes = VRP.aco.twoOpt(routeNodes)
        }

        var routeNodesArray = routeNodes.toArray
        //          (k.routeMatrixArray).update(routeIndex, routeNodesArray)
        k.routeMatrix = routeNodes :: k.routeMatrix //reversed later, below

        //Pretty Print stuff:

        //denorm means denormalised:

        System.out.println("")
        System.out.println("")
        System.out.println("route # " + pheroMIndex + ": ")
        //          System.out.println("routeNodes: " + routeNodes)
        //denorm:
        System.out.print("routeNodes: ")
        routeNodes.foreach { n =>
          System.out.print(n + 1 + " ")
        }
        System.out.println("")

        var cumulativeDist: Double = 0
        var cumulativeDemand: Int = 0
        var dist: Double = 0
        for (i <- 0 to routeNodesArray.length - 1) { //CHECK this "length-2" everywhere, does it calculate ALL distances, even for last node in list?
          if (i == 0) {
            var distToDepot = distMatrixArray(depot)(routeNodesArray(i))
            var pheroToDepot = routePheroMatrix(depot)(routeNodesArray(i))
            //              System.out.print(distToDepot + "d, " + pheroToDepot + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")

            //denorm:
            System.out.print(distToDepot + "d, " + pheroToDepot + "p - " + (routeNodesArray(i) + 1) + "(" + VRP.nodeDemandArray(routeNodesArray(i)) + ")")
            dist += distToDepot
          } else if (i == (routeNodesArray.length - 1)) {
            dist = distMatrixArray(routeNodesArray(i - 1))(routeNodesArray(i))
            var phero = routePheroMatrix(routeNodesArray(i - 1))(routeNodesArray(i))
            var distToDepot: Double = distMatrixArray(routeNodesArray(i))(depot)
            var pheroToDepot = routePheroMatrix(routeNodesArray(i))(depot)
            //              System.out.print(" - " + dist + "d, " + phero + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")" + " - " + distToDepot + "d, " + pheroToDepot + "p")

            //denorm:
            System.out.print(" - " + dist + "d, " + phero + "p - " + (routeNodesArray(i) + 1) + "(" + VRP.nodeDemandArray(routeNodesArray(i)) + ")" + " - " + distToDepot + "d, " + pheroToDepot + "p")

            dist += distToDepot
          } else {
            dist = distMatrixArray(routeNodesArray(i - 1))(routeNodesArray(i))
            var phero = routePheroMatrix(routeNodesArray(i - 1))(routeNodesArray(i))
            //              System.out.print(" - " + dist + "d, " + phero + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")

            //denorm:
            System.out.print(" - " + dist + "d, " + phero + "p - " + (routeNodesArray(i) + 1) + "(" + VRP.nodeDemandArray(routeNodesArray(i)) + ")")
          }

          cumulativeDist += dist
          cumulativeDemand += VRP.nodeDemandArray(routeNodesArray(i))
        }
        assert(cumulativeDemand <= VRP.capacity)

        k.solutionDistance += cumulativeDist

        System.out.println("")
        System.out.println("cumulative Distance (for each route): " + cumulativeDist)
        System.out.println("cumulative Demand: " + cumulativeDemand)

        //denorm:
        System.out.println("")
        System.out.print("k.visitedNodes: ")
        for (n <- 0 to k.visitedNodesVector.size - 1) {
          //        k.visitedNodes.foreach { n =>
          System.out.print((k.visitedNodesVector.get(n) + 1) + " ")
        }
        System.out.println("")
        var visitedNodesList: List[Int] = Nil //(k.visitedNodesVector.toArray).toList
        for (i <- 0 to k.visitedNodesVector.size - 1) {
          visitedNodesList = k.visitedNodesVector.get(i) :: visitedNodesList
        }
        visitedNodesList.reverse
        val sorted = visitedNodesList.sortWith((e1, e2) => (e1 < e2))

        System.out.print("sorted k.visitedNodesVector: ")
        sorted.foreach { n =>
          System.out.print((n + 1) + " ")
        }
        System.out.println("")

        //eqn (3):

        var currentPheroMatrix = (pheroMVector.get(pheroMIndex)) //was above the try block //pheroMatrix for route routeIndex //(changed from val to var, just in case)

        try {
          //          var currentPheroMatrix = (pheroMVector.get(routeIndex)) //was above the try block //pheroMatrix for route routeIndex //(changed from val to var, just in case)
          val currentRouteNodes = k.routeMatrix(pheroMIndex) //the nodes in route routeIndex, i.e. routeNodes for route routeIndex.

          for (i <- 0 to currentRouteNodes.length - 2) { //for each pair of nodes in this route:
            val newPhero: Double = (1 - alpha) * { currentPheroMatrix(currentRouteNodes(i))(currentRouteNodes(i + 1)).getValue } + alpha * t0
            currentPheroMatrix(currentRouteNodes(i))(currentRouteNodes(i + 1)).setValue(newPhero)
          } //ok indices

          //also update phero on arcs from and to the depot:

          val newPheroFromDepot = (1 - alpha) * { (currentPheroMatrix(depot)(currentRouteNodes(0))).getValue } + alpha * t0
          (currentPheroMatrix(depot)(currentRouteNodes(0))).setValue(newPheroFromDepot)

          val newPheroToDepot = (1 - alpha) * { (currentPheroMatrix(currentRouteNodes(currentRouteNodes.length - 1))(depot)).getValue } + alpha * t0
          (currentPheroMatrix(currentRouteNodes(currentRouteNodes.length - 1))(depot)).setValue(newPheroToDepot)

        } catch {
          case indexOutOfBounds: IndexOutOfBoundsException =>
            System.out.println("This solution has fewer routes than the others")
        }

        //MULTIPLE COLONY STUFF:

        if (nextThread == null) {
          if (k.visitedNodesSet.size < dimension & !k.eligibleNodesSet.isEmpty) {
            nextThread = new RouteThread(pheroMIndex + 1, ants)
            nextThread.setParams(hetaMatrix, pheroMVector) //TODO!: is pheroMVector the same as at the beginning of the class, or does it change for each thread?
            nextThread.start()
          }
        }

        //        } //end of  outer while

        //go back to the depot once eligibleNodes = Nil:
        k.nextDestination = depot
        //        k.visitedNodes = k.nextDestination :: k.visitedNodes

        k.visitedNodesSet.add(k.nextDestination)
        k.visitedNodesVector.add(k.nextDestination) //TODO: check added at the right place!

        //        k.visitedNodes = k.visitedNodes.reverse
        k.routeMatrix = k.routeMatrix.reverse

        System.out.println("solutionDistance: " + k.solutionDistance)

        // eqn(3) moved inside route loop for the PRL version
        
      } // end of k.synchronized
    } //end of ant loop
    if (nextThread != null) {
      System.err.println("About to join: " + nextThread)
      nextThread.join()
    }
  }
}